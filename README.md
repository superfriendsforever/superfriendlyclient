# Super Friendly Client
---

## Setting up a Dev Environment

To set up the development environment, the following software will need to be installed on your machine.

 * Unity 2018.3.0f2 *( Preferebly through [Unity Hub](https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe?_ga=2.126655497.503948174.1546030889-1802293141.1544754353) )*
 
 * [Git for Windows](https://git-scm.com/download/win)
 
 * [Steam](https://steamcdn-a.akamaihd.net/client/installer/SteamSetup.exe) & SteamVR  *( SteamVR can only be installed from inside the Steam Client )* 
 
Once these programs are installed, the repo can be cloned using git https or git ssh. If using git https, your bitbucket username and password will be required for every pull or push, if using git ssh then your ssh keys will need
to be set up before trying to pull.

To set up SSH keys, go to your BitBucket settings and go to the SSH Keys tab, there you can find links for generating and adding an SSH key.

---

## First time setup

Once the repository is cloned, the file ./update_libraries.ps1 must be ran to load all submodules, if the Unity project is loaded before this file is ran then you risk the scenes being corrupted due to missing files.

To run the initial setup, in powershell run the following command:

`powershell -ExecutionPolicy ByPass -File .\update_libraries.ps1`

** This command MUST be ran from inside the root directory of the repository and MUST be ran before running Unity for the first time after cloning the repository **

---

## Gallery

![Nebula Skybox at the Hub](https://i.imgur.com/Na3ujqa.png)

---